# README #

This README written by Dennis Wong at 2016/11/28 12:46.

### How to set up? ###

* get [SourceTree](https://www.sourcetreeapp.com/)
* clone this repo on SourceTree to anywhere on the local drive
* get code editor to start development, example: [Notepad++](https://notepad-plus-plus.org/download/v7.1.html), [Sublime Text 3](https://www.sublimetext.com/3)

### How to commit ###
* create a new branch for yourself
* add something or modify some code
* type in the comment of commit in SourceTree
* press 'Stage All' in SourceTree
* press 'Commit' in SourceTree

### How to start server ###
* check whether the database structure is the latest, if not, go to Migration of DB
* type "python manage.py runserver" in console to start the server
* url is "localhost:8000"

### When you modify model.py, how to apply to database ###
* type "python manage.py makemigrations staffdevelopmentsystem" in console
* type "python manage.py migrate" in console

### Migration of DB ###
* type "python manage.py migrate" in console

### About default database data ###
* I only provides category data, user group data as well as some user data:
* Account name:
1. superuser
2. admin
3. hr
4. instructor
5. participant
* All the passwords are 1234abcd
* The default data will be automatically imported in migration

### What have we done ###
* Instructor part (Create course and sub-class, manage course, sub-class, module and component)
* Participant part (All course, add and drop, course list, module list, component list)
* Participant can see images, text and youtube video in components
* Admin can assign user group to user
* HR can add, edit and delete enrollment
* Participant can take quizs at the end of each module
* User progress implemented with quiz management and submission system

### Where is the uploaded file of components if I am an instructor? ###
* The path is "/media/"
* The filename is the filename when uploaded
* The filename and extension when you upload will be stored in database
* The actual path and filename will be displayed in admin page [here](http://localhost:8000/adminstaffdevelopmentsystem/component/) inside component objects, you can directly upload file here