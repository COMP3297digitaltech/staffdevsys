from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.core.validators import validate_comma_separated_integer_list
from django.utils.timezone import now

# Create your models here.

class Category(models.Model):
	name = models.CharField(max_length=200)
	def __str__(self):
		return self.name

class Course(models.Model):
	name = models.CharField(max_length=200)
	categoryID = models.ForeignKey(Category, on_delete=models.CASCADE)
	limit = models.IntegerField(default=30)
	def __str__(self):
		return self.name

class Class(models.Model):
	courseID = models.ForeignKey(Course)
	description = models.CharField(max_length=200)
	code = models.CharField(max_length=2)
	instructorID = models.ForeignKey(User, on_delete=models.CASCADE)
	status = models.IntegerField(default=0)
	# status
	# 0 = closed
	# 1 = open for enrollment
	# 2 = in progress
	# 3 = ended
	remain = models.IntegerField(default=30)

class Module(models.Model):
	courseID = models.ForeignKey(Course, on_delete=models.CASCADE)
	classID = models.ForeignKey(Class, on_delete=models.CASCADE)
	name = models.CharField(max_length=200)
	description = models.CharField(max_length=200)
	order = models.IntegerField(default=1)
	def __str__(self):
		return self.name

class Component(models.Model):
	courseID = models.ForeignKey(Course, on_delete=models.CASCADE)
	classID = models.ForeignKey(Class, on_delete=models.CASCADE)
	moduleID = models.ForeignKey(Module, on_delete=models.CASCADE)
	ext = models.CharField(max_length=10)
	filename = models.CharField(max_length=100)
	order = models.IntegerField(default=1)
	type = models.IntegerField(default=0)
	document = models.FileField(upload_to='/',  default='settings.MEDIA_ROOT')
	# type
	# 0 = files
	# 1 = images
	# 2 = text
	# 3 = youtube url
	text = models.CharField(max_length=2048)
	youtube = models.CharField(max_length=2048)

class CourseAssignment(models.Model):
	userID = models.ForeignKey(User, on_delete=models.CASCADE)
	courseID = models.ForeignKey(Course, on_delete=models.CASCADE)
	classID = models.ForeignKey(Class, on_delete=models.CASCADE)

class UserProgress(models.Model):
	userID = models.ForeignKey(User, on_delete=models.CASCADE)
	courseID = models.ForeignKey(Course, on_delete=models.CASCADE)
	classID = models.ForeignKey(Class, on_delete=models.CASCADE)
	passedModule = models.CharField(max_length=1024, validators=[validate_comma_separated_integer_list])
	nowModuleID = models.ForeignKey(Module, on_delete=models.CASCADE, null=True)
	incompleteModule = models.CharField(max_length=1024, validators=[validate_comma_separated_integer_list])
	updateTime = models.DateTimeField(default=now)

class Question(models.Model):
	courseID =  models.ForeignKey(Course, on_delete=models.CASCADE)
	classID = models.ForeignKey(Class, on_delete=models.CASCADE)
	moduleID = models.ForeignKey(Module, on_delete=models.CASCADE)
	order = models.IntegerField(default=1)
	correctAnswer = models.IntegerField(default=1)
	question = models.CharField(max_length=1024)

class Answer(models.Model):
	courseID =  models.ForeignKey(Course, on_delete=models.CASCADE)
	classID = models.ForeignKey(Class, on_delete=models.CASCADE)
	moduleID = models.ForeignKey(Module, on_delete=models.CASCADE)
	questionID = models.ForeignKey(Question, on_delete=models.CASCADE)
	answer = models.CharField(max_length=1024)

class Submission(models.Model):
	userID =  models.ForeignKey(User, on_delete=models.CASCADE)
	courseID =  models.ForeignKey(Course, on_delete=models.CASCADE)
	classID = models.ForeignKey(Class, on_delete=models.CASCADE)
	moduleID = models.ForeignKey(Module, on_delete=models.CASCADE)
	userAnswer = models.CharField(max_length=1024, validators=[validate_comma_separated_integer_list])
	dateTime = models.DateTimeField(default=now)
	marks = models.IntegerField(default=0)