from .models import Category
from .models import Course
from .models import Class
from .models import Module
from .models import Component
from .models import CourseAssignment
from .models import UserProgress
from .models import Question
from .models import Answer
from .models import Submission

from django.conf import settings

from django.contrib.auth import authenticate, login
from django.contrib.auth import views as auth_views
from django.contrib.auth.models import Group
from django.contrib.auth.models import Permission
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType

from django.core import files 
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage

from django.db.models import F

from django.http import FileResponse
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import StreamingHttpResponse

from django.shortcuts import render

from wsgiref.util import FileWrapper

import os
import mimetypes
import logging

logger = logging.getLogger(__name__)

def index(request):
	return HttpResponseRedirect('/login')

def reg(request):
	return render(request, 'register.html')

def top(request):

	user = User.objects.filter(id=request.user.id)[0]
	groups = Group.objects.all()
	return render(request, 'home.html', {'user' : user, 'groups' : groups})

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/login')

def auth(request):
	username = request.POST['username']
	password = request.POST['password']
	user = authenticate(username=username, password=password)
	if user is not None:
		login(request, user)
		return HttpResponseRedirect('/top')
	else:
		return HttpResponseRedirect('/login')

def register(request):
	username = request.POST['username']
	email = request.POST['email']
	password = request.POST['password']
	user = User.objects.create_user(username, email, password)
	user.save()

	g = Group.objects.get(name='Participant') 
	g.user_set.add(user)

	user = authenticate(username=username, password=password)
	if user is not None:
		login(request, user)
		return HttpResponseRedirect('/top')
	else:
		return HttpResponseRedirect('/login')

# ===================================================================================
#                                   UI response
# ===================================================================================

# =======================================
#                Instructor
# =======================================

# UI response: Instructor: Create a course with sub-class
@login_required
def instructor_create(request):
    all_categories = Category.objects.order_by('name').all()	
    return render(request, 'instructor_create.html', {'categories': all_categories})

# UI response: Instructor: Create a sub-class of existing course
@login_required
def instructor_create_subclass(request):
    coursesObject = Course.objects.order_by('name').all()	
    return render(request, 'instructor_create_subclass.html', {'courses': coursesObject})

# UI response: Instructor: Course List of Management
@login_required
def instructor_manage(request):
	instructorID = request.user.id
	instructorOject = User.objects.filter(id=instructorID)[0]

	all_categories = Category.objects.order_by('name').all()
	count = Class.objects.prefetch_related('courseID').filter(instructorID = instructorOject).count()
	all_classes = Class.objects.prefetch_related('courseID').filter(instructorID = instructorOject).all()
	return render(request, 'instructor_manage.html', {'categories': all_categories, 'classes' : all_classes, 'count' : count})

# UI response: Instructor: Course page of course managing
@login_required
def instructor_manage_course(request):
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.prefetch_related('courseID').filter(id=classID).filter(courseID=courseObject)[0]
	ModuleObject = Module.objects.prefetch_related('courseID').prefetch_related('classID').filter(courseID=courseObject).filter(classID=classObject).order_by('order')

	all_categories = Category.objects.order_by('name').all()
	
	if(Module.objects.filter(courseID=courseObject).filter(classID=classObject).count()<1):
		return render(request, 'instructor_manage_course.html', {'class': classObject, 'categories': all_categories, 'modules' : ModuleObject, 'latestOrder' : 0})
	else:
		latestModule = Module.objects.filter(courseID=courseObject).filter(classID=classObject).order_by("-order") [0]
		return render(request, 'instructor_manage_course.html', {'class': classObject, 'categories': all_categories, 'modules' : ModuleObject, 'latestOrder' : latestModule.order})

# UI response: Instructor: Module management
@login_required
def instructor_manage_module(request):
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)
	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.prefetch_related('courseID').filter(id=classID).filter(courseID=courseObject)[0]
	ModuleObject = Module.objects.prefetch_related('courseID').prefetch_related('classID').filter(courseID=courseObject).filter(classID=classObject).filter(id=moduleID)[0]
	ComponentObject = Component.objects.filter(moduleID=ModuleObject).order_by('order')

	questionCount = Question.objects.filter(moduleID=ModuleObject).count()

	if(Component.objects.filter(moduleID=ModuleObject).count()<1):
		return render(request, 'instructor_manage_module.html', {'components':ComponentObject, 'module': ModuleObject, 'latestOrder' : 0, 'questionCount' : questionCount})
	else:
		latestComponent = Component.objects.filter(moduleID=ModuleObject).order_by("-order") [0]
		return render(request, 'instructor_manage_module.html', {'components':ComponentObject, 'module': ModuleObject, 'latestOrder' : latestComponent.order, 'questionCount' : questionCount})

# UI response: Instructor: Quiz management
@login_required
def instructor_manage_quiz(request):
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.prefetch_related('courseID').filter(id=classID).filter(courseID=courseObject)[0]
	ModuleObject = Module.objects.prefetch_related('courseID').prefetch_related('classID').filter(courseID=courseObject).filter(classID=classObject).filter(id=moduleID)[0]

	questionCount = Question.objects.filter(moduleID=ModuleObject).count()
	questionObject = Question.objects.filter(moduleID=ModuleObject).order_by('order')
	
	latestQuestion = Question.objects.filter(moduleID=ModuleObject).order_by("-order") [0]
	return render(request, 'instructor_manage_quiz.html', {'questions': questionObject, 'module': ModuleObject, 'latestOrder' : latestQuestion.order, 'questionCount' : questionCount})

# UI response: Instructor: Question management
@login_required
def instructor_manage_question(request):
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)
	questionID = request.GET.get("questionID", "")
	questionID = int('0' + questionID)
	moduleObject = Module.objects.prefetch_related('courseID').prefetch_related('classID').filter(id=moduleID)[0]

	questionObject = Question.objects.prefetch_related('courseID').prefetch_related('classID').prefetch_related('moduleID').filter(id=questionID)[0]

	answerObject = Answer.objects.filter(questionID=questionObject).all()
	
	answerCount = Answer.objects.filter(questionID=questionObject).count()

	return render(request, 'instructor_manage_question.html', {'module' : moduleObject, 'question': questionObject, 'answers': answerObject, 'answerCount' : answerCount})

# UI response: Instructor: Component management
@login_required
def instructor_manage_component(request):
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)
	componentID = request.GET.get("componentID", "")
	componentID = int('0' + componentID)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.prefetch_related('courseID').filter(id=classID).filter(courseID=courseObject)[0]
	ModuleObject = Module.objects.prefetch_related('courseID').prefetch_related('classID').filter(courseID=courseObject).filter(classID=classObject).filter(id=moduleID)[0]
	ComponentObject = Component.objects.filter(id=componentID).prefetch_related('courseID').prefetch_related('classID').prefetch_related('moduleID') [0]
	
	return render(request, 'instructor_manage_component.html', {'component':ComponentObject})

# =======================================
#                Participant
# =======================================

# UI response: Participant: All courses list
@login_required
def participant_all(request):
	coursesObject = Course.objects.prefetch_related().all()
	return render(request, 'participant_all.html', {'courses' : coursesObject})	

# UI response: Participant: Select category to add course page
@login_required	
def participant_add(request):
    all_categories = Category.objects.order_by('name').all()
    return render(request, 'participant_add.html', {'categories': all_categories})

# UI response: Participant: Drop course page
@login_required
def participant_drop(request):
	participantID = request.user.id	

	iparticipantOject = User.objects.filter(id=participantID)[0]

	allCourseAssignmentObject = CourseAssignment.objects.prefetch_related().filter(userID=iparticipantOject)
	classID = allCourseAssignmentObject.values('classID')
	inProgressClassCount = Class.objects.prefetch_related('courseID').filter(id__in=classID).filter(status=2).count()
	openedClassCount = Class.objects.prefetch_related('courseID').filter(id__in=classID).filter(status=1).count()
	if inProgressClassCount > 0:
		currentCount = inProgressClassCount
		currentClass = Class.objects.prefetch_related('courseID').filter(id__in=classID).filter(status=2).first()
		return render(request, 'participant_drop.html', {'count' : currentCount, 'class' : currentClass})	
	elif openedClassCount> 0:
		currentCount = openedClassCount
		currentClass = Class.objects.prefetch_related('courseID').filter(id__in=classID).filter(status=1).first()
		return render(request, 'participant_drop.html', {'count' : currentCount, 'class' : currentClass})
	else:
		currentCount = 0
		currentClass = None
		return render(request, 'participant_drop.html', {'count' : currentCount})	

# UI response: Participant: Course list of category
@login_required
def participant_add_category(request):
	if request.method == 'POST':
		category = request.POST.get('category')
		categoryObject=Category.objects.filter(name=category)[0]
		all_course = Course.objects.prefetch_related('categoryID').filter(categoryID=categoryObject).all()
	return render(request, 'participant_add_category.html',{'selectedCategory': category ,'course': all_course} )

# UI response: Participant: Course and sub-class details of selected course
@login_required
def participant_add_course(request):
	if request.method == 'POST':
		participantID = request.user.id	
		iparticipantOject = User.objects.filter(id=participantID)[0]
		category = request.POST.get('category')
		courseID = request.POST.get("courseID","")
		courseObject = Course.objects.filter(id=courseID)[0]
		subClassObject = Class.objects.prefetch_related('courseID').filter(courseID=courseObject).all()
		
		flag = 0
		courseAssignmentObject = CourseAssignment.objects.prefetch_related().filter(userID=iparticipantOject)
		for courseAssignmentEach in courseAssignmentObject:
			if courseAssignmentEach.classID.status != 3:
				flag = 1
		if flag == 1:
			for courseAssignmentEach in courseAssignmentObject:
				if courseAssignmentEach.classID.status != 3:
					currentID = courseAssignmentEach.classID.id
			return render(request, 'participant_add_course.html',{'currentCourse':courseObject ,'classes': subClassObject, 'count' : flag, 'currentID' : currentID})
		else:
			return render(request, 'participant_add_course.html',{'currentCourse':courseObject ,'classes': subClassObject, 'count' : flag})

# UI response: Participant: Current and previous enrolled courses list
@login_required		
def participant_course_list(request):
	participantID = request.user.id	
	participantOject = User.objects.filter(id=participantID)[0]
	allCourseAssignmentObject = CourseAssignment.objects.prefetch_related().filter(userID=participantOject)
	classID = allCourseAssignmentObject.values('classID')
	inProgressClassCount = Class.objects.prefetch_related('courseID').filter(id__in=classID).filter(status=2).count()
	openedClassCount = Class.objects.prefetch_related('courseID').filter(id__in=classID).filter(status=1).count()
	if inProgressClassCount > 0:
		currentCount = inProgressClassCount
		currentClass = Class.objects.prefetch_related('courseID').filter(id__in=classID).filter(status=2).first()
	elif openedClassCount> 0:
		currentCount = openedClassCount
		currentClass = Class.objects.prefetch_related('courseID').filter(id__in=classID).filter(status=1).first()
	else:
		currentCount = 0
		currentClass = None
	endedClassCount = Class.objects.prefetch_related('courseID').filter(id__in=classID).filter(status=3).count()
	endedClass = Class.objects.prefetch_related('courseID').filter(id__in=classID).filter(status=3)
	
	return render(request, 'participant_courses_list.html', {'currentCount' : currentCount, 'current' : currentClass, 'endedClassCount' : endedClassCount, 'endedClass' : endedClass})

# UI response: Participant: Course Page
@login_required		
def participant_course(request):
	participantID = request.user.id	
	iparticipantOject = User.objects.filter(id=participantID)[0]

	classID=request.GET.get("classID")
	classObject = Class.objects.filter(id=classID)[0]

	userProgressObject = UserProgress.objects.filter(classID=classObject).filter(userID=iparticipantOject)[0]

	moduleCount = Module.objects.filter(classID=classObject).count()

	if moduleCount > 0:
		moduleObject = Module.objects.filter(classID=classObject)
		incompleteModuleList = [int('0' + x.strip()) for x in userProgressObject.incompleteModule.split(',')]
		passModuleList = [int('0' + x.strip()) for x in userProgressObject.passedModule.split(',')]
		return render(request, 'participant_courses.html', {'class' : classObject, 'moduleCount' : moduleCount, 'modules' : moduleObject, 'userProgress' : userProgressObject, 'incomplete' : incompleteModuleList, 'pass' : passModuleList})
	else:
		return render(request, 'participant_courses.html', {'class' : classObject, 'moduleCount' : moduleCount})

# UI response: Participant: Module Details
@login_required		
def participant_module(request):
	participantID = request.user.id	
	iparticipantOject = User.objects.filter(id=participantID)[0]

	classID=request.GET.get("classID")
	courseID=request.GET.get("courseID")
	moduleID=request.GET.get("moduleID")
	courseObject=Course.objects.filter(id=courseID)[0]
	classObject=Class.objects.filter(courseID=courseObject).filter(id=classID)[0]
	moduleObject = Module.objects.filter(courseID=courseObject).filter(classID=classObject).filter(id=moduleID)[0]

	componentObject = Component.objects.filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).all()
	
	questionCount = Question.objects.filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).count()
	
	submissionCount = Submission.objects.filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).filter(userID=iparticipantOject).count()

	if submissionCount > 0:
		submissionObject = Submission.objects.filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).filter(userID=iparticipantOject).order_by('-dateTime')[0]
		return render(request, 'participant_courses_module.html', {'module' : moduleObject, 'components' : componentObject , 'questionCount' : questionCount, 'passMark' : questionCount/2, 'submissionCount' : submissionCount, 'lastSubmission' : submissionObject})
	else:
		return render(request, 'participant_courses_module.html', {'module' : moduleObject, 'components' : componentObject , 'questionCount' : questionCount, 'submissionCount' : submissionCount})

# UI response: Participant: Quiz Page
@login_required		
def participant_quiz(request):
	classID=request.GET.get("classID")
	courseID=request.GET.get("courseID")
	moduleID=request.GET.get("moduleID")
	courseObject=Course.objects.filter(id=courseID)[0]
	classObject=Class.objects.filter(courseID=courseObject).filter(id=classID)[0]
	moduleObject = Module.objects.filter(courseID=courseObject).filter(classID=classObject).filter(id=moduleID)[0]

	questionCount = Question.objects.filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).count()
	
	questionObject = Question.objects.filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).all()
	answerObject = Answer.objects.prefetch_related('questionID').filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).all()

	return render(request, 'participant_courses_quiz.html', {'module' : moduleObject, 'questionCount' : questionCount, 'questions' : questionObject, 'answers' : answerObject})

# UI response: Participant: Previous quiz details
@login_required
def participant_quiz_review(request):
	classID=request.GET.get("classID")
	courseID=request.GET.get("courseID")
	moduleID=request.GET.get("moduleID")
	submissionID=request.GET.get("submissionID")
	courseObject=Course.objects.filter(id=courseID)[0]
	classObject=Class.objects.filter(courseID=courseObject).filter(id=classID)[0]
	moduleObject = Module.objects.filter(courseID=courseObject).filter(classID=classObject).filter(id=moduleID)[0]

	questionCount = Question.objects.filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).count()
	
	questionObject = Question.objects.filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).all()
	answerObject = Answer.objects.prefetch_related('questionID').filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).all()

	submissionObject = Submission.objects.filter(id=submissionID)[0]
	answerList = [int('0' + x.strip()) for x in submissionObject.userAnswer.split(',')]

	return render(request, 'participant_courses_quiz_review.html', {'module' : moduleObject, 'questionCount' : questionCount, 'passMark' : questionCount/2, 'questions' : questionObject, 'answers' : answerObject, 'answerList' : answerList, 'submission' : submissionObject})

# UI response: Participant: Previous quiz list
@login_required		
def participant_quiz_list(request):
	participantID = request.user.id	
	iparticipantOject = User.objects.filter(id=participantID)[0]

	classID=request.GET.get("classID")
	courseID=request.GET.get("courseID")
	moduleID=request.GET.get("moduleID")

	courseObject=Course.objects.filter(id=courseID)[0]
	classObject=Class.objects.filter(courseID=courseObject).filter(id=classID)[0]
	moduleObject = Module.objects.filter(courseID=courseObject).filter(classID=classObject).filter(id=moduleID)[0]

	submissionObject = Submission.objects.filter(moduleID=moduleObject).filter(userID=iparticipantOject).all()
	
	questionCount = Question.objects.filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).count()
	
	return render(request, 'participant_courses_quiz_list.html', {'module' : moduleObject, 'submissions' : submissionObject, 'questionCount' : questionCount, 'passMark' : questionCount/2})

# =======================================
#                  Admin
# =======================================

# UI response: Admin: User list
@login_required
def admin_user(request):
	users = User.objects.all()
	groups = Group.objects.all()
	return render(request, 'admin_user.html', {'users' : users, 'groups' : groups})	

# =======================================
#                   HR
# =======================================

# UI response: HR: Staff list
@login_required
def hr_staff(request):
	enrollments = CourseAssignment.objects.all()
	return render(request, 'hr_staff.html', {'enrollments' : enrollments})	

# UI response: HR: Staff detail editing page 1
@login_required
def hr_staff_edit(request):
	enrollmentID = request.POST.get("enrollmentID", "")
	enrollmentID = int('0' + enrollmentID)
	enrollmentObject = CourseAssignment.objects.get(id=enrollmentID) 

	userID = request.POST.get("userID", "")
	userID = int('0' + userID)
	userObject = User.objects.get(id=userID) 

	courses = Course.objects.all()
	return render(request, 'hr_staff_edit.html', {'enrollment' : enrollmentObject, 'user' : userObject, 'courses' : courses})	

# UI response: HR: Staff detail editing page 2
@login_required
def hr_staff_edit_next(request):
	enrollmentID = request.POST.get("enrollmentID", "")
	enrollmentID = int('0' + enrollmentID)
	enrollmentObject = CourseAssignment.objects.get(id=enrollmentID) 

	userID = request.POST.get("userID", "")
	userID = int('0' + userID)
	userObject = User.objects.get(id=userID) 

	courseID = request.POST.get("course", "")
	courseID = int('0' + courseID)
	courseObject = Course.objects.get(id=courseID) 

	classObject = Class.objects.filter(courseID=courseObject)
	return render(request, 'hr_staff_edit_sub.html', {'enrollment' : enrollmentObject, 'user' : userObject, 'course' : courseObject, 'classes' : classObject})	

# UI response: HR: Staff adding page 1
@login_required
def hr_staff_add(request):
	users = User.objects.all()
	courses = Course.objects.all()
	return render(request, 'hr_staff_add.html', {'users' : users, 'courses' : courses})	

# UI response: HR: Staff adding page 2
@login_required
def hr_staff_add_next(request):
	userID = request.POST.get("user", "")
	userID = int('0' + userID)
	userObject = User.objects.get(id=userID) 

	courseID = request.POST.get("course", "")
	courseID = int('0' + courseID)
	courseObject = Course.objects.get(id=courseID) 

	classObject = Class.objects.filter(courseID=courseObject)
	return render(request, 'hr_staff_add_sub.html', {'user' : userObject, 'course' : courseObject, 'classes' : classObject})	

# =======================================
#                   Misc
# =======================================

# UI response: File link for download response
@login_required
def file(request):
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)
	componentID = request.GET.get("componentID", "")
	componentID = int('0' + componentID)

	file_name = request.GET.get("filename", "")
	file_path = 'media/' + file_name

	fsock = open(file_path,"rb")

	file_mimetype = mimetypes.guess_type(file_path)

	file_wrapper = FileWrapper(open(file_path,'rb'))
	response = HttpResponse(file_wrapper, content_type=file_mimetype )
	response['X-Sendfile'] = file_path
	response['Content-Length'] = os.stat(file_path).st_size
	response['Content-Disposition'] = 'attachment; filename=' + file_name
	return response

# ===================================================================================
#                                  Database data transfer
# ===================================================================================

# =======================================
#                Enrollment
# =======================================

# DB: Enrollment add
@login_required
def db_enroll(request):
	name = request.POST.get("name")
	classID=request.POST.get("classID")
	courseID=request.POST.get("courseID")
	courseObject=Course.objects.filter(id=courseID)[0]
	classObject=Class.objects.filter(courseID=courseObject).filter(id=classID)[0]
	participantID = request.user.id	
	iparticipantOject = User.objects.filter(id=participantID)[0]
	flag = False
	courseAssignmentObject = CourseAssignment.objects.prefetch_related().filter(userID=iparticipantOject)
	for courseAssignmentEach in courseAssignmentObject:
		if courseAssignmentEach.classID.status != 3:
			flag = True
	if flag:
		return render(request, 'participant_enroll_fail.html' )
	else:
		firstModuleObject = Module.objects.filter(courseID=courseObject).filter(classID=classObject).order_by('id').first()
		incompletedModuleList = []
		incompletedModuleCount = Module.objects.filter(courseID=courseObject).filter(classID=classObject).count()
		if incompletedModuleCount > 0:
			incompletedModuleObject = Module.objects.filter(courseID=courseObject).filter(classID=classObject).exclude(id=firstModuleObject.id).order_by('id')
			for module in incompletedModuleObject:
				incompletedModuleList.append(module.id)
			incompletedModuleStr = ','.join(map(str, incompletedModuleList))
		else:
			incompletedModuleStr = ""

		userProgressObject = UserProgress(userID=iparticipantOject, courseID=courseObject, classID=classObject, nowModuleID=firstModuleObject, incompleteModule=incompletedModuleStr)
		userProgressObject.save()

		addcourseObject = CourseAssignment(classID=classObject, courseID=courseObject , userID = iparticipantOject)
		addcourseObject.save()
		Class.objects.filter(courseID=courseObject).filter(id=classID).update(remain=F('remain')-1)
		return render(request, 'participant_enroll_finish.html', {"name" : name } )

# DB: Enrollment drop
@login_required
def db_drop(request):
	name = request.POST.get("name")
	courseID=request.POST.get("courseID")
	classID=request.POST.get("classID")
	courseObject=Course.objects.filter(id=courseID)[0]
	classObject=Class.objects.filter(courseID=courseObject).filter(id=classID)[0]
	participantID = request.user.id	
	iparticipantOject = User.objects.filter(id=participantID)[0]
	count = CourseAssignment.objects.filter(userID=iparticipantOject).filter(courseID=courseObject).filter(classID=classObject).count()
	if count != 1:
		return render(request, 'participant_drop_fail.html', {"name" : name } )
	else:
		UserProgress.objects.filter(userID=iparticipantOject).filter(courseID=courseObject).filter(classID=classObject).delete()
		Submission.objects.filter(userID=iparticipantOject).filter(courseID=courseObject).filter(classID=classObject).delete()

		CourseAssignment.objects.filter(userID=iparticipantOject).filter(courseID=courseObject).filter(classID=classObject).delete()
		Class.objects.filter(courseID=courseObject).filter(id=classID).update(remain=F('remain')+1)
		return render(request, 'participant_drop_finish.html', {"name" : name } )

# =======================================
#         Course and sub-class
# =======================================

# DB: Create course
@login_required
def db_create(request):
	name = request.POST.get("name", "")
	sub_class = request.POST.get("code", "")
	description = request.POST.get("description", "")
	category = request.POST.get("category", "")
	limit = request.POST.get("limit", "")
	instructorID = request.user.id

	categoryOject = Category.objects.filter(id=category)[0]
	instructorOject = User.objects.filter(id=instructorID)[0]

	courseObject = Course(name=name, limit=limit, categoryID = categoryOject)
	courseObject.save()

	classObject = Class(description=description, code = sub_class, courseID = courseObject, instructorID = instructorOject, remain=limit)
	classObject.save()

	moduleObject = Module(courseID = courseObject, classID = classObject, name = "Module 1", description = "Module 1 description", order = 1)
	moduleObject.save()

	return render(request, 'instructor_create_finish.html', {"name" : name , "code" : sub_class, "type" : "course"} )

# DB: Create sub-class
@login_required
def db_create_subclass(request):
	sub_class = request.POST.get("code", "")
	description = request.POST.get("description", "")
	course = request.POST.get("course", "")
	instructorID = request.user.id

	courseObject = Course.objects.filter(name=course)[0]
	instructorOject = User.objects.filter(id=instructorID)[0]
	limit = courseObject.limit
	name = courseObject.name

	classObject = Class(description=description, code = sub_class, courseID = courseObject, instructorID = instructorOject, remain=limit)
	classObject.save()

	moduleObject = Module(courseID = courseObject, classID = classObject, name = "Module 1", description = "Module 1 description", order = 1)
	moduleObject.save()

	return render(request, 'instructor_create_finish.html', {"name" : name , "code" : sub_class, "type" : "sub"} )

# DB: Edit course details
@login_required
def db_edit(request):
	courseID = request.POST.get("courseID","")
	classID = request.POST.get("classID","")
	name = request.POST.get("name", "")
	category = request.POST.get("category", "")
	limit = request.POST.get("limit", "")
	instructorID = request.user.id

	categoryOject = Category.objects.filter(name=category)[0]
	instructorOject = User.objects.filter(id=instructorID)[0]
	courseObject = Course.objects.filter(id=courseID)[0]

	courseEdit = Course.objects.filter(id=courseID).update(name=name, categoryID=categoryOject, limit=limit)
	return HttpResponseRedirect('/manage/course?courseID='+ str(courseID) + '&classID=' + str(classID))

# DB: Edit sub-class details
@login_required
def db_edit_subclass(request):
	courseID = request.POST.get("courseID","")
	classID = request.POST.get("classID","")
	sub_class = request.POST.get("code", "")
	description = request.POST.get("description", "")
	status = request.POST.get("status", 0)
	instructorID = request.user.id

	instructorOject = User.objects.filter(id=instructorID)[0]
	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.prefetch_related('courseID').filter(id=classID).filter(courseID=courseObject)[0]

	classEdit = Class.objects.filter(courseID=courseObject).filter(id=classID).update(code=sub_class, description=description, status=status)
	return HttpResponseRedirect('/manage/course?courseID='+ str(courseID) + '&classID=' + str(classID))

# DB: Delete sub-class
@login_required
def db_delete(request):
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	course = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(courseID=course).filter(id=classID)[0]
	Class.objects.filter(id=classID).filter(courseID=course).delete()
	count = Class.objects.filter(courseID=course).count()
	if count <= 0:
		Course.objects.filter(id=courseID).delete()
		return render(request, 'instructor_delete_finish.html', {"name" : course.name, "redirect" : "/manage", "type": "Course+Class"} )
	else:
		return render(request, 'instructor_delete_finish.html', {"class": classObject.code, "name" : course.name, "redirect" : "/manage", "type": "Class"} )

# =======================================
#                Modules
# =======================================

# DB: Add module
@login_required
def db_module_add(request):
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	latestOrder = request.GET.get("latestOrder", "")
	latestOrder = int('0' + latestOrder)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]

	name = "Module "+str(latestOrder+1)
	description = "Module "+str(latestOrder+1) + " description"
	moduleObject = Module(courseID=courseObject, classID=classObject, name=name, description=description, order = latestOrder+1)
	moduleObject.save()

	module = Module.objects.filter(classID=classID).filter(courseID=courseObject).filter(order=latestOrder+1)[0]
	
	questionObject = Question(courseID=courseObject, classID=classObject, moduleID=module)
	questionObject.save()

	question = Question.objects.filter(classID=classID).filter(courseID=courseObject).filter(moduleID=module)[0]
	
	answerObject = Answer(courseID=courseObject, classID=classObject, moduleID=module, questionID=question)
	answerObject.save()
	answerObject2 = Answer(courseID=courseObject, classID=classObject, moduleID=module, questionID=question)
	answerObject2.save()

	Question.objects.filter(id=question.id).update(correctAnswer=answerObject.id)
	
	return HttpResponseRedirect('/manage/course?courseID='+ str(courseID) + '&classID=' + str(classID))

# DB: Delete module
@login_required
def db_module_delete(request):
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]
	module = Module.objects.filter(id=moduleID).filter(courseID=courseObject).filter(classID=classObject)[0]

	module_changed = Module.objects.filter(courseID=courseObject).filter(classID=classObject).filter(order__gt = module.order).update(order=F('order')-1)

	Module.objects.filter(id=moduleID).filter(courseID=courseObject).filter(classID=classObject).delete()

	return HttpResponseRedirect("/manage/course?courseID=" + str(courseID) + "&classID=" + str(classID))

# DB: Edit module details
@login_required
def db_module_edit(request):
	courseID = request.POST.get("courseID","")
	courseID = int('0' + courseID)
	classID = request.POST.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.POST.get("moduleID","")
	moduleID = int('0' + moduleID)
	name = request.POST.get("name", "")
	description = request.POST.get("description", "")

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]
	ModuleObject = Module.objects.filter(courseID=courseObject).filter(classID=classObject).filter(id=moduleID)[0]

	moduleEdit = Module.objects.filter(courseID=courseObject).filter(classID=classObject).filter(id=moduleID).update(name=name, description=description)
	
	return HttpResponseRedirect('/manage/module?courseID='+ str(courseID) + '&classID=' + str(classID) + '&moduleID=' + str(moduleID))

# DB: Edit module order
@login_required
def db_module_order(request):
	type = request.GET.get("type", "")
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]

	nowModuleID = moduleID
	nowModuleObjects = Module.objects.filter(courseID=courseObject).filter(classID=classObject).filter(id=nowModuleID)
	nowModuleOrder = nowModuleObjects[0].order
	if type == "up":
		upperModuleOrder = nowModuleOrder-1
		upperModuleObjects = Module.objects.filter(courseID=courseObject).filter(classID=classObject).filter(order=upperModuleOrder)
		upperModuleID = upperModuleObjects[0].id

		Module.objects.filter(id=nowModuleID).update(order=F('order')-1)
		Module.objects.filter(id=upperModuleID).update(order=F('order')+1)
	if type == "down":
		lowerModuleOrder = nowModuleOrder+1
		lowerModuleObjects = Module.objects.filter(courseID=courseObject).filter(classID=classObject).filter(order=lowerModuleOrder)
		lowerModuleID = lowerModuleObjects[0].id

		Module.objects.filter(id=nowModuleID).update(order=F('order')+1)
		Module.objects.filter(id=lowerModuleID).update(order=F('order')-1)

	return HttpResponseRedirect('/manage/course?courseID='+ str(courseID) + '&classID=' + str(classID))

# =======================================
#                Components
# =======================================

# DB: Add component
@login_required
def db_component_add(request):
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)
	latestOrder = request.GET.get("latestOrder", "")
	latestOrder = int('0' + latestOrder)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]
	moduleObject = Module.objects.filter(id=moduleID).filter(courseID=courseObject).filter(classID=classObject)[0]

	componentObject = Component(courseID=courseObject, classID=classObject, moduleID=moduleObject, order = latestOrder+1)
	componentObject.save()
	
	return HttpResponseRedirect('/manage/module?courseID='+ str(courseID) + '&classID=' + str(classID) + '&moduleID=' + str(moduleID))

# DB: Delete component
@login_required
def db_component_delete(request):
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)
	componentID = request.GET.get("componentID", "")
	componentID = int('0' + componentID)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]
	moduleObject = Module.objects.filter(id=moduleID).filter(courseID=courseObject).filter(classID=classObject)[0]
	component = Component.objects.filter(id=componentID).filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject)[0]

	component_changed = Component.objects.filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).filter(order__gt = component.order).update(order=F('order')-1)

	Component.objects.filter(id=componentID).filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).delete()

	return HttpResponseRedirect('/manage/module?courseID=' + str(courseID) + "&classID=" + str(classID) + "&moduleID=" + str(moduleID))

# DB: Edit component order
@login_required
def db_component_order(request):
	type = request.GET.get("type", "")
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)
	componentID = request.GET.get("componentID", "")
	componentID = int('0' + componentID)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]
	moduleObject = Module.objects.filter(id=moduleID).filter(courseID=courseObject).filter(classID=classObject)[0]

	nowComponentID = componentID
	nowComponentObjects = Component.objects.filter(id=nowComponentID).filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject)
	nowComponentOrder = nowComponentObjects[0].order
	if type == "up":
		upperComponentOrder = nowComponentOrder-1
		upperComponentObjects = Component.objects.filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).filter(order=upperComponentOrder)
		upperComponentID = upperComponentObjects[0].id

		Component.objects.filter(id=nowComponentID).update(order=F('order')-1)
		Component.objects.filter(id=upperComponentID).update(order=F('order')+1)
	if type == "down":
		lowerComponentOrder = nowComponentOrder+1
		lowerComponentObjects = Component.objects.filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).filter(order=lowerComponentOrder)
		lowerComponentID = lowerComponentObjects[0].id

		Component.objects.filter(id=nowComponentID).update(order=F('order')+1)
		Component.objects.filter(id=lowerComponentID).update(order=F('order')-1)

	return HttpResponseRedirect('/manage/module?courseID='+ str(courseID) + '&classID=' + str(classID) + '&moduleID=' + str(moduleID))

# DB: Edit component content: upload a file/image
@login_required
def db_component_upload(request):
	courseID = request.POST.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.POST.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.POST.get("moduleID", "")
	moduleID = int('0' + moduleID)
	componentID = request.POST.get("componentID", "")
	componentID = int('0' + componentID)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]
	moduleObject = Module.objects.filter(id=moduleID).filter(courseID=courseObject).filter(classID=classObject)[0]

	componentObject = Component.objects.filter(id=componentID).filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject)[0]
	path = componentObject.document.name
	try:
		os.remove(path[1:])
	except OSError:
		pass

	component = Component.objects.filter(id=componentID).filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject)[0]

	data = request.FILES['file']
	filename, ext = os.path.splitext(data.name)

	path = filename+ext
	try:
		os.remove(path)
	except OSError:
		pass
	file = default_storage.save(path, ContentFile(data.read()))
	tmp_file = os.path.join(settings.MEDIA_URL, file)

	if ext.find("png") != -1:
		type = 1
	elif ext.find("PNG") != -1:
		type = 1
	elif ext.find("jpg") != -1:
		type = 1
	elif ext.find("JPG") != -1:
		type = 1
	elif ext.find("jpeg") != -1:
		type = 1
	elif ext.find("JPEG") != -1:
		type = 1
	elif ext.find("bmp") != -1:
		type = 1
	elif ext.find("BMP") != -1:
		type = 1
	else:
		type = 0

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]
	moduleObject = Module.objects.filter(id=moduleID).filter(courseID=courseObject).filter(classID=classObject)[0]
	Component.objects.filter(id=componentID).filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).update(filename=filename, ext=ext, type=type, document=tmp_file, text="", youtube="")

	return HttpResponseRedirect('/manage/component?courseID='+ str(courseID) + '&classID=' + str(classID) + '&moduleID=' + str(moduleID)+ '&componentID=' + str(componentID))

# DB: Edit component content: edit text
@login_required
def db_component_text(request):
	courseID = request.POST.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.POST.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.POST.get("moduleID", "")
	moduleID = int('0' + moduleID)
	componentID = request.POST.get("componentID", "")
	componentID = int('0' + componentID)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]
	moduleObject = Module.objects.filter(id=moduleID).filter(courseID=courseObject).filter(classID=classObject)[0]
	componentObject = Component.objects.filter(id=componentID).filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject)[0]
	path = componentObject.document.name
	try:
		os.remove(path[1:])
	except OSError:
		pass

	type = 2

	text = request.POST.get("text", "")
	Component.objects.filter(id=componentID).filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).update(filename="", ext="", type=type, document="", text=text, youtube="")

	return HttpResponseRedirect('/manage/component?courseID='+ str(courseID) + '&classID=' + str(classID) + '&moduleID=' + str(moduleID)+ '&componentID=' + str(componentID))

# DB: Edit component content: edit youtube link
@login_required
def db_component_youtube(request):
	courseID = request.POST.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.POST.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.POST.get("moduleID", "")
	moduleID = int('0' + moduleID)
	componentID = request.POST.get("componentID", "")
	componentID = int('0' + componentID)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]
	moduleObject = Module.objects.filter(id=moduleID).filter(courseID=courseObject).filter(classID=classObject)[0]
	componentObject = Component.objects.filter(id=componentID).filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject)[0]
	path = componentObject.document.name
	try:
		os.remove(path[1:])
	except OSError:
		pass

	type = 3

	url = request.POST.get("youtube", "")
	Component.objects.filter(id=componentID).filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).update(filename="", ext="", type=type, document="", text="", youtube=url)

	return HttpResponseRedirect('/manage/component?courseID='+ str(courseID) + '&classID=' + str(classID) + '&moduleID=' + str(moduleID)+ '&componentID=' + str(componentID))

# DB: Edit component content: clear file/image of component
@login_required
def db_component_clean(request):
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)
	componentID = request.GET.get("componentID", "")
	componentID = int('0' + componentID)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]
	moduleObject = Module.objects.filter(id=moduleID).filter(courseID=courseObject).filter(classID=classObject)[0]

	componentObject = Component.objects.filter(id=componentID).filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject)[0]
	path = componentObject.document.name
	try:
		os.remove(path[1:])
	except OSError:
		pass

	Component.objects.filter(id=componentID).filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).update(filename="", ext="", document="")

	return HttpResponseRedirect('/manage/component?courseID='+ str(courseID) + '&classID=' + str(classID) + '&moduleID=' + str(moduleID)+ '&componentID=' + str(componentID))

# =======================================
#                Quizs
# =======================================

# DB: Quiz submission handling
@login_required
def db_quiz_submit(request):
	userID = request.user.id
	userObject = User.objects.get(id=userID) 

	courseID = request.POST.get("courseID", "")
	courseID = int('0' + courseID)
	courseObject = Course.objects.get(id=courseID) 

	classID = request.POST.get("classID", "")
	classID = int('0' + classID)
	classObject = Class.objects.get(id=classID) 
	
	moduleID = request.POST.get("moduleID", "")
	moduleID = int('0' + moduleID)
	moduleObject = Module.objects.get(id=moduleID) 

	questionObject = Question.objects.filter(moduleID=moduleObject)
	questionCount = Question.objects.filter(moduleID=moduleObject).count()
	marks = 0

	answerList = []
	for question in questionObject:
		answer = request.POST.getlist('answer'+str(question.id))
		answerList.append(answer[0])
		if str(question.correctAnswer) == str(answer[0]):
			marks = marks + 1
	
	answerStr = ','.join(map(str, answerList))

	submissionObject = Submission(userID=userObject, courseID=courseObject, classID=classObject, moduleID=moduleObject, userAnswer=answerStr, marks=marks)
	submissionObject.save()

	if marks >= questionCount/2:
		nowModuleObject = moduleObject

		incompletedModuleList = []
		incompletedModuleCount = Module.objects.filter(courseID=courseObject).filter(classID=classObject).filter(id__gt=nowModuleObject.id).count()
		incompletedModuleObject = Module.objects.filter(courseID=courseObject).filter(classID=classObject).filter(id__gt=nowModuleObject.id)
		for module in incompletedModuleObject:
			if incompletedModuleObject[0].id != module.id:
				incompletedModuleList.append(module.id)
		
		incompletedModuleStr = ','.join(map(str, incompletedModuleList))

		passModuleList = []
		passModuleObject = Module.objects.filter(courseID=courseObject).filter(classID=classObject).filter(id__lte=nowModuleObject.id)
		for module in passModuleObject:
			passModuleList.append(module.id)
		
		passModuleStr = ','.join(map(str, passModuleList))
		if incompletedModuleCount > 0:
			UserProgress.objects.filter(userID=userObject, courseID=courseObject, classID=classObject).update(nowModuleID=incompletedModuleObject[0], incompleteModule=incompletedModuleStr, passedModule=passModuleStr)
		else:
			UserProgress.objects.filter(userID=userObject, courseID=courseObject, classID=classObject).update(nowModuleID="", incompleteModule=incompletedModuleStr, passedModule=passModuleStr)
		
	return HttpResponseRedirect('/course/quiz/review?&courseID=' + str(courseID) + '&classID=' + str(classID) + '&moduleID=' + str(moduleID) + '&submissionID=' + str(submissionObject.id))

# =======================================
#                Questions
# =======================================

# DB: Add question
@login_required
def db_question_add(request):
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)
	latestOrder = request.GET.get("latestOrder", "")
	latestOrder = int('0' + latestOrder)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]
	moduleObject = Module.objects.filter(id=moduleID).filter(courseID=courseObject).filter(classID=classObject)[0]

	questionObject = Question(courseID=courseObject, classID=classObject, moduleID=moduleObject, order = latestOrder+1)
	questionObject.save()
	
	question = Question.objects.filter(classID=classObject).filter(courseID=courseObject).filter(moduleID=moduleObject).filter(order = latestOrder+1)[0]
	
	answerObject = Answer(courseID=courseObject, classID=classObject, moduleID=moduleObject, questionID=question)
	answerObject.save()
	answerObject2 = Answer(courseID=courseObject, classID=classObject, moduleID=moduleObject, questionID=question)
	answerObject2.save()
	
	Question.objects.filter(moduleID=moduleObject).filter(order = latestOrder+1).update(correctAnswer=answerObject.id)

	return HttpResponseRedirect('/manage/quiz?courseID='+ str(courseID) + '&classID=' + str(classID) + '&moduleID=' + str(moduleID))

# DB: Delete question
@login_required
def db_question_delete(request):
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)
	questionID = request.GET.get("questionID", "")
	questionID = int('0' + questionID)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]
	moduleObject = Module.objects.filter(id=moduleID).filter(courseID=courseObject).filter(classID=classObject)[0]
	question = Question.objects.filter(id=questionID).filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject)[0]

	question_changed = Question.objects.filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).filter(order__gt = question.order).update(order=F('order')-1)

	Question.objects.filter(id=questionID).filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).delete()

	return HttpResponseRedirect('/manage/question?moduleID=' + str(moduleID))

# DB: Edit question order
@login_required
def db_question_order(request):
	type = request.GET.get("type", "")
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)
	questionID = request.GET.get("questionID", "")
	questionID = int('0' + questionID)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]
	moduleObject = Module.objects.filter(id=moduleID).filter(courseID=courseObject).filter(classID=classObject)[0]

	nowQuestionID = questionID
	nowQuestionObjects = Question.objects.filter(id=nowQuestionID).filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject)
	nowQuestionOrder = nowQuestionObjects[0].order
	if type == "up":
		upperQuestionOrder = nowQuestionOrder-1
		upperQuestionObjects = Question.objects.filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).filter(order=upperQuestionOrder)
		upperQuestionID = upperQuestionObjects[0].id

		Question.objects.filter(id=nowQuestionID).update(order=F('order')-1)
		Question.objects.filter(id=upperQuestionID).update(order=F('order')+1)
	if type == "down":
		lowerQuestionOrder = nowQuestionOrder+1
		lowerQuestionObjects = Question.objects.filter(courseID=courseObject).filter(classID=classObject).filter(moduleID=moduleObject).filter(order=lowerQuestionOrder)
		lowerQuestionID = lowerQuestionObjects[0].id

		Question.objects.filter(id=nowQuestionID).update(order=F('order')+1)
		Question.objects.filter(id=lowerQuestionID).update(order=F('order')-1)

	return HttpResponseRedirect('/manage/quiz?courseID='+ str(courseID) + '&classID=' + str(classID) + '&moduleID=' + str(moduleID))

# DB: Edit question content
@login_required
def db_question_edit(request):
	moduleID = request.POST.get("moduleID", "")
	moduleID = int('0' + moduleID)
	questionID = request.POST.get("questionID", "")
	questionID = int('0' + questionID)
	question = request.POST.get("question", "")

	Question.objects.filter(id=questionID).update(question=question)

	return HttpResponseRedirect('/manage/question?moduleID=' + str(moduleID) + '&questionID=' + str(questionID))

# =======================================
#                Answers
# =======================================

# DB: Edit answer content
@login_required
def db_answer_edit(request):
	moduleID = request.POST.get("moduleID", "")
	moduleID = int('0' + moduleID)
	questionID = request.POST.get("questionID", "")
	questionID = int('0' + questionID)
	answerIDs = request.POST.getlist('answerID')
	answer = request.POST.getlist('answer')
	for index, answerID in enumerate(answerIDs):
	    Answer.objects.filter(questionID=questionID).filter(id=answerID).update(answer=answer[index])

	return HttpResponseRedirect('/manage/question?moduleID=' + str(moduleID) + '&questionID=' + str(questionID))

# DB: Set answer as correct answer
@login_required
def db_answer_correct(request):
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)
	questionID = request.GET.get("questionID", "")
	questionID = int('0' + questionID)
	answerID = request.GET.get("answerID", "")
	answerID = int('0' + answerID)

	Question.objects.filter(id=questionID).update(correctAnswer=answerID)

	return HttpResponseRedirect('/manage/question?moduleID=' + str(moduleID) + '&questionID=' + str(questionID))

# DB: Add answer
@login_required
def db_answer_add(request):
	courseID = request.GET.get("courseID", "")
	courseID = int('0' + courseID)
	classID = request.GET.get("classID", "")
	classID = int('0' + classID)
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)
	questionID = request.GET.get("questionID", "")
	questionID = int('0' + questionID)

	courseObject = Course.objects.filter(id=courseID)[0]
	classObject = Class.objects.filter(id=classID).filter(courseID=courseObject)[0]
	moduleObject = Module.objects.filter(id=moduleID).filter(courseID=courseObject).filter(classID=classObject)[0]
	questionObject = Question.objects.filter(id=questionID).filter(moduleID=moduleObject).filter(courseID=courseObject).filter(classID=classObject)[0]

	answerObject = Answer(courseID=courseObject, classID=classObject, moduleID=moduleObject, questionID=questionObject)
	answerObject.save()

	return HttpResponseRedirect('/manage/question?moduleID=' + str(moduleID) + '&questionID=' + str(questionID))

# DB: Delete answer
@login_required
def db_answer_delete(request):
	moduleID = request.GET.get("moduleID", "")
	moduleID = int('0' + moduleID)
	questionID = request.GET.get("questionID", "")
	questionID = int('0' + questionID)
	answerID = request.GET.get("answerID", "")
	answerID = int('0' + answerID)

	Answer.objects.filter(id=answerID).delete()

	return HttpResponseRedirect('/manage/question?moduleID=' + str(moduleID) + '&questionID=' + str(questionID))

# =======================================
#             User group
# =======================================

# DB: Add/remove user group from user
@login_required
def db_group(request):
	userID = request.GET.get("userID", "")
	userID = int('0' + userID)
	groupID = request.GET.get("groupID", "")
	groupID = int('0' + groupID)
	type = request.GET.get("type", "")
	type = int('0' + type)

	u = User.objects.get(id=userID) 
	if type == 0:
		g = Group.objects.get(id=groupID) 
		g.user_set.add(u)
	elif type == 1:
		g = Group.objects.get(id=groupID) 
		g.user_set.remove(u)

	return HttpResponseRedirect('/user')

# =======================================
#                 Staffs
# =======================================

# DB: Delete staff
@login_required
def db_staff_delete(request):
	enrollmentID = request.POST.get("enrollmentID", "")
	enrollmentID = int('0' + enrollmentID)

	CourseAssignment.objects.filter(id=enrollmentID).delete()

	return HttpResponseRedirect('/staff')

# DB: Edit staff details
@login_required
def db_staff_edit(request):
	enrollmentID = request.POST.get("enrollmentID", "")
	enrollmentID = int('0' + enrollmentID)

	userID = request.POST.get("userID", "")
	userID = int('0' + userID)
	userObject = User.objects.get(id=userID) 

	courseID = request.POST.get("courseID", "")
	courseID = int('0' + courseID)
	courseObject = Course.objects.get(id=courseID) 

	classID = request.POST.get("class", "")
	classID = int('0' + classID)
	classObject = Class.objects.get(id=classID) 

	CourseAssignment.objects.filter(id=enrollmentID).update(userID=userObject, courseID=courseObject, classID=classObject)

	return HttpResponseRedirect('/staff')

# DB: Add staff
@login_required
def db_staff_add(request):
	userID = request.POST.get("userID", "")
	userID = int('0' + userID)
	userObject = User.objects.get(id=userID) 

	courseID = request.POST.get("courseID", "")
	courseID = int('0' + courseID)
	courseObject = Course.objects.get(id=courseID) 

	classID = request.POST.get("class", "")
	classID = int('0' + classID)
	classObject = Class.objects.get(id=classID) 

	courseAssignmentObject = CourseAssignment(userID=userObject, courseID=courseObject, classID=classObject)
	courseAssignmentObject.save()
	
	return HttpResponseRedirect('/staff')
