# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-07 04:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('staffdevelopmentsystem', '0005_auto_20161029_1836'),
    ]

    operations = [
        migrations.AddField(
            model_name='class',
            name='status',
            field=models.IntegerField(default=0),
        ),
    ]
