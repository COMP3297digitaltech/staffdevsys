# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-29 09:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('staffdevelopmentsystem', '0002_module_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='component',
            name='path',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
