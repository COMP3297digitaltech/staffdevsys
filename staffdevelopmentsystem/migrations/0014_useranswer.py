# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-29 15:46
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('staffdevelopmentsystem', '0013_auto_20161129_1418'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserAnswer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uesrAnswer', models.IntegerField(default=1)),
                ('classID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='staffdevelopmentsystem.Class')),
                ('courseID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='staffdevelopmentsystem.Course')),
                ('moduleID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='staffdevelopmentsystem.Module')),
                ('questionID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='staffdevelopmentsystem.Question')),
            ],
        ),
    ]
