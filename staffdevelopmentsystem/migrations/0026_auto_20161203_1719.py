# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-12-03 08:19
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('staffdevelopmentsystem', '0025_auto_20161203_1629'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprogress',
            name='nowModuleID',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='staffdevelopmentsystem.Module'),
        ),
    ]
