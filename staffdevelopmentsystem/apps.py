from django.apps import AppConfig


class StaffdevelopmentsystemConfig(AppConfig):
    name = 'staffdevelopmentsystem'
