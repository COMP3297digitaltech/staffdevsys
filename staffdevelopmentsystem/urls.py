from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.auth.models import Permission, User

from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType

from django.contrib.auth import authenticate
from django.conf import settings
admin.site.site_header = settings.ADMIN_SITE_HEADER

from . import views
urlpatterns = [
    url(r'^login$', auth_views.login, kwargs={'redirect_authenticated_user': True, 'template_name': 'login.html'}, name='login'),
    url(r'^logout$', auth_views.logout, {'next_page': '/'}, name='logout'),
	url(r'^auth', views.auth, name='auth'),
	url(r'^register', views.register, name='register'),
	url(r'^reg', views.reg, name='reg'),
	url(r'^top', views.top, name='top'),

	url(r'^file', views.file, name='file'),
	url(r'^create/subclass', views.instructor_create_subclass, name='instructor_create_subclass'),
	url(r'^create', views.instructor_create, name='instructor_create'),

	url(r'^manage/course', views.instructor_manage_course, name='instructor_manage_course'),
	url(r'^manage/module', views.instructor_manage_module, name='instructor_manage_module'),
	url(r'^manage/quiz', views.instructor_manage_quiz, name='instructor_manage_quiz'),
	url(r'^manage/question', views.instructor_manage_question, name='instructor_manage_question'),
	url(r'^manage/component', views.instructor_manage_component, name='instructor_manage_component'),
	url(r'^manage', views.instructor_manage, name='instructor_manage'),

	url(r'^all', views.participant_all, name='participant_all'),

	url(r'^add/category', views.participant_add_category, name='participant_add_category'),
    url(r'^add/course', views.participant_add_course, name='participant_add_course'),
	url(r'^add/finish', views.db_enroll, name='participant_add_finish'),
	url(r'^add', views.participant_add, name='participant_add'),

	url(r'^drop/finish', views.db_drop, name='participant_drop_finish'),
	url(r'^drop', views.participant_drop, name='participant_drop'),

	url(r'^course/module', views.participant_module, name='participant/course/module'),
	url(r'^course/quiz/review', views.participant_quiz_review, name='participant/course/quiz/review'),
	url(r'^course/quiz/list', views.participant_quiz_list, name='participant/course/quiz/list'),
	url(r'^course/quiz', views.participant_quiz, name='participant/course/quiz'),
	url(r'^course/detail', views.participant_course, name='participant/course'),
	url(r'^course', views.participant_course_list, name='participant/course/list'),

	url(r'^user', views.admin_user, name='admin_user'),

	url(r'^staff/edit/next', views.hr_staff_edit_next, name='hr_staff_edit_next'),
	url(r'^staff/edit', views.hr_staff_edit, name='hr_staff_edit'),
	url(r'^staff/add/next', views.hr_staff_add_next, name='hr_staff_add_next'),
	url(r'^staff/add', views.hr_staff_add, name='hr_staff_add'),
	url(r'^staff', views.hr_staff, name='hr_staff'),

	url(r'^db/create/subclass', views.db_create_subclass, name='db_create_sub_class'),
	url(r'^db/create', views.db_create, name='db_create'),

	url(r'^db/edit/subclass', views.db_edit_subclass, name='db_edit_subclass'),
	url(r'^db/edit', views.db_edit, name='db_edit'),
	
	url(r'^db/delete', views.db_delete, name='db_delete'),

	url(r'^db/module/delete', views.db_module_delete, name='db_module_delete'),
	url(r'^db/module/add', views.db_module_add, name='db_module_add'),
	url(r'^db/module/edit', views.db_module_edit, name='db_module_edit'),
	url(r'^db/module/order', views.db_module_order, name='db_module_order'),

	url(r'^db/component/add', views.db_component_add, name='db_component_add'),
	url(r'^db/component/order', views.db_component_order, name='db_component_order'),
	url(r'^db/component/delete', views.db_component_delete, name='db_component_delete'),
	url(r'^db/component/upload', views.db_component_upload, name='db_component_upload'),
	url(r'^db/component/clean', views.db_component_clean, name='db_component_clean'),
	url(r'^db/component/text', views.db_component_text, name='db_component_text'),
	url(r'^db/component/youtube', views.db_component_youtube, name='db_component_youtube'),

	url(r'^db/question/add', views.db_question_add, name='db_question_add'),
	url(r'^db/question/edit', views.db_question_edit, name='db_question_edit'),
	url(r'^db/question/order', views.db_question_order, name='db_question_order'),
	url(r'^db/question/delete', views.db_question_delete, name='db_question_delete'),

	url(r'^db/answer/add', views.db_answer_add, name='db_answer_add'),
	url(r'^db/answer/delete', views.db_answer_delete, name='db_answer_delete'),
	url(r'^db/answer/edit', views.db_answer_edit, name='db_answer_edit'),
	url(r'^db/answer/correct', views.db_answer_correct, name='db_answer_correct'),
	
	url(r'^db/quiz/submit', views.db_quiz_submit, name='db_quiz_submit'),

	url(r'^db/group', views.db_group, name='db_group'),

	url(r'^db/staff/delete', views.db_staff_delete, name='db_staff_delete'),
	url(r'^db/staff/edit', views.db_staff_edit, name='db_staff_edit'),
	url(r'^db/staff/add', views.db_staff_add, name='db_staff_add'),

	url(r'^', views.index, name='index'),
]
